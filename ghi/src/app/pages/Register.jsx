import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { useCreateAccountMutation, useGetUserTokenQuery, useLoginMutation } from "../apiSlice"



const Register = () => {
    const navigate = useNavigate()
    const [login, loginStatus] = useLoginMutation()
    const [createAccount, result] = useCreateAccountMutation()
    const {data: account, isLoading} = useGetUserTokenQuery()
    const [passwordConfirm, setPasswordConfirm] = useState('')
    const [error, setError] = useState('')

    const [formData, setFormData] = useState({
        email: "",
        password: "",
        full_name: "",
        height_in_inches: "",
        weight_in_pounds: "",
        sex: ""
    })

    useEffect(() => {
        if (account?.account) navigate('/')
    }, [loginStatus, account, result, isLoading])
    
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        setFormData({
                ...formData,
                [inputName]: value
            })
    }

    const handlePasswordConfirm = (e) => {
        const value = e.target.value
        setPasswordConfirm(value)

    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        if ( formData.password == passwordConfirm) {
            const data =  await createAccount(formData)
            if (data) {
                login({
                    email: formData.email,
                    password: formData.password
                    })
                }   
            } else {
                setError("Passwords do not match!")
            }
    }


    return (
        <>
            <div className="container mx-8">
                <h1>Register Here</h1>
                <form className="form-floating" onSubmit={handleSubmit}>
                    {error && <div className="alert alert-danger" role="alert">
                        {error}
                    </div>}
                    <div className="form-floating mb-3">
                        <input type="email" className="form-control" id="email" value={formData.email} onChange={handleFormChange} name="email" placeholder="email" />
                        <label htmlFor="email">Email Address</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input type="text" className="form-control" id="full_name" value={formData.full_name} onChange={handleFormChange} placeholder="full_name" name="full_name"/>
                        <label htmlFor="full_name">Full Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input type="password" className="form-control" id="password" value={formData.password} onChange={handleFormChange} placeholder="password" name="password" />
                        <label htmlFor="password">Password</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input type="password" className="form-control" id="confirm_password" value={passwordConfirm} onChange={handlePasswordConfirm} placeholder="confirm_password" />
                        <label htmlFor="confirm_password">Confirm Password</label>
                    </div>
                    <div className="form-floating mb-3">   
                        <input type="number" className="form-control" id="height" value={formData.height_in_inches} onChange={handleFormChange} placeholder="height" name="height_in_inches" />
                        <label htmlFor="height">Height in inches</label>
                    </div> 
                    <div className="form-floating mb-3"> 
                        <input type="number" className="form-control" id="number" value={formData.weight_in_pounds} onChange={handleFormChange} placeholder="weight" name="weight_in_pounds" />
                        <label htmlFor="weight">Weight in pounds</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="sex">Sex</label> 
                        <select value={formData.sex} onChange={handleFormChange} className="form-select form-select-sm" name="sex">
                            <option defaultValue="">-- Select Sex --</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>
                    <div className="form-floating mb-3"> 
                        <button className="btn btn-primary" type='submit'>Register Account</button> 
                    </div>
                </form>
            </div>
        </>
    )
}

export default Register 
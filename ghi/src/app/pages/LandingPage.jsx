import '../css/styles.css';
import {useState, useEffect} from 'react';
import RecommendedWorkout from '../components/RecommendedWorkout';
import { useGetUserTokenQuery } from '../apiSlice';


const LandingPage = () => {

    const { data: account, isLoading, isSuccess } = useGetUserTokenQuery();

    const [authenticated, setAuthenticated] = useState(false);

    useEffect(() => {
        if (isSuccess && account) {
            setAuthenticated(true);
        } else {
            setAuthenticated(false);
        }
    }, [isSuccess, account]);

    if (isLoading) return <div>Loading...</div>;

    return (
        <div>
            <header className="hero-section text-center">
                <h1 className="main-page-title">Welcome to SWOLEDAWGS</h1>
                <p className="lead">Track your workouts and crush your fitness goals</p>
                    <img src="../swole_dawg.avif" alt="Gym Logo" />
            </header>

            {authenticated && <RecommendedWorkout /> }

            {!authenticated && (
                <>
            <section className="features-section container mt-5">
                <h2 className="text-center mb-5">Our Features</h2>
                <ul className="list-unstyled text-center">
                    <li>Log your daily workouts, including exercises, sets, reps, and weights.</li>
                    <li>Check out your workouts via calendar.</li>
                    <li>Create your very own custom workouts.</li>
                    <li>Recommended workout feature: based on logs of previous workouts done during the week.</li>
                </ul>
            </section>

            <section className="testimonial-section py-5">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 offset-md-3">
                            <div className="testimonial text-center">
                                <h3>Testomonials</h3>
                                <p>SWOLEDAWGS helped me go from looking like a boulder to a 175lb ripped shiny bold headed sexy beast! </p>
                                <p className="author">- Alex Rubin</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </>
            )}
        </div>
    );
};

export default LandingPage;

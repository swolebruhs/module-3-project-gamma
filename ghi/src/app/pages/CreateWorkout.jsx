import { useEffect, useState } from 'react'
import {
    useGetExerciseListQuery,
    useCreateExerciseSliceMutation,
    useCreateWorkoutMutation,
    useGetUserTokenQuery,
} from '../apiSlice'
import { useNavigate } from 'react-router-dom'



const ExerciseForm = ({ exercise, onCreate }) => {
    const [formData, setFormData] = useState({
        exercise_id: exercise.id,
        sets: 3,
        reps: 10,
    })
    const[ submitted, setSubmitted ] = useState(false)
    
    const handleExerciseSubmit = (e) => {
        e.preventDefault()
        if (onCreate) {
            onCreate(formData)
            setSubmitted(true)
            setTimeout(() => {
                setSubmitted(false);
            },
            2000);
        }
    }

    const handleInputChange = (e) => {
        const name = e.target.name
        const value = e.target.value
        setFormData({ ...formData, [name]: value })
    }

    return (
        <form onSubmit={handleExerciseSubmit}>
            <div className="card mb-3">
                <div className="card-body">
                    <h5 className="card-title">{exercise.name}</h5>
                    <h6 className="card-subtitle mb-2 text-body-secondary">
                        {exercise.body_group}
                    </h6>
                    <p className="card-text">{exercise.descriptions}</p>
                    <div className="row g-3 align-items-center">
                        <div className="col-auto">
                            <label htmlFor="sets" className="form-label">
                                Sets
                            </label>
                        </div>
                        <div className="col-auto">
                            <input
                                value={formData.sets}
                                onChange={handleInputChange}
                                name="sets"
                                type="number"
                                className="form-control"
                                id="sets"
                            />
                        </div>
                    </div>
                    <div className="row g-3 align-items-center pb-2">
                        <br />
                        <div className="col-auto">
                            <label htmlFor="reps" className="form-label">
                                Reps
                            </label>
                        </div>
                        <div className="col-auto">
                            <input
                                value={formData.reps}
                                onChange={handleInputChange}
                                name="reps"
                                type="number"
                                className="form-control"
                                id="reps"
                            />
                        </div>
                        <br />
                    </div>
                    <div className="row g-3 align-items-center">
                        {submitted && (
                            <div className="d-inline-flex p-2 rounded-4 text-success bg-success-subtle">
                                Exercise added!
                            </div>
                        )}
                        <div className="col-auto">
                            <button type="submit" className="btn btn-primary">
                                Add to Workout
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    )
}

const CreateWorkout = () => {
    const navigate = useNavigate()
    const [workoutName, setWorkoutName] = useState('')
    const { data: exercises, isLoading } = useGetExerciseListQuery()
    const [createExerciseSlice] = useCreateExerciseSliceMutation()
    const [createWorkout] = useCreateWorkoutMutation()
    const [exerciseList, setExerciseList] = useState([])
    const [filterExercises, setFilterExercises] = useState([])
    const [filterExerciseType, setfilterExerciseType] = useState()
    const { data: account, isLoading: accountLoading } = useGetUserTokenQuery()

    useEffect(() => {
        if (!account && !accountLoading) navigate('/') 
    }, [account, accountLoading])
    
    const handleCreateExercise = async (formData) => {
        try {
            const response = await createExerciseSlice(formData)
            const createdExerciseSlice = response.data
            setExerciseList([...exerciseList, createdExerciseSlice])
        } catch (e) {
            console.log('error', e)
        }
    }

    const handleExerciseDelete = (e) => {
        const id = e.target.id;
        const newEx = exerciseList.filter((ex)=> ex.id != id);
        setExerciseList(newEx);
    };
    
    const handleInputChange = (e) => {
        const value = e.target.value
        setWorkoutName(value)
    }
    
    const handleWorkoutSubmit = async (e) => {
        e.preventDefault()
        
        let user_id = account.account.id
        
        try {
            const exercise_id_list = exerciseList.map((exercise) => exercise.id)
            const workout = {
                name: workoutName,
                exercise_ids: exercise_id_list,
                user_id: user_id
            }
            const workoutResponse = await createWorkout(workout)
            if (workoutResponse) navigate('/workouts')
        } catch (e) {
    console.log('error', e)
}
}

const filterExerciseList = (e) => {
    const selectedType = e.target.value
    setfilterExerciseType(selectedType)
    if (selectedType == '') setFilterExercises([])
    if (selectedType) {
        const filteredExercises = exercises.filter(
            (exercise) => exercise.body_group === selectedType
            )
            setFilterExercises(filteredExercises)
        }
    }
    if (isLoading) return <div>Loading...</div>
    
    return (
        <>
            <div className="container mx-8">
            <h1>Create Workout</h1>
                <form onSubmit={handleWorkoutSubmit}>
                    <div className="mb-3">
                        <label htmlFor="workout-name" className="form-label">
                            Workout Name
                        </label>
                        <input
                            value={workoutName}
                            onChange={handleInputChange}
                            name="workout-name"
                            id="workout-name"
                            type="text"
                            className="form-control"
                            required
                        />
                    </div>
                    {exerciseList.length > 0 && (
                        <table className="table">
                            <thead className="table-dark">
                                <tr>
                                    <th>Exercise</th>
                                    <th>Sets</th>
                                    <th>Reps</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {exerciseList.map((exer) => {
                                    return (
                                        <tr key={exer.id}>
                                            <td>{exer.exercise.name}</td>
                                            <td>{exer.sets}</td>
                                            <td>{exer.reps}</td>
                                            <td>
                                                <button
                                                    onClick={handleExerciseDelete}
                                                    type="button"
                                                    className="btn btn-outline-danger"
                                                    id={exer.id}
                                                >
                                                    Remove from workout
                                                </button>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    )}
                    <br></br>
                    <button className="btn btn-primary">Create Workout</button>
                </form>
            </div>
            <div className="container mt-4">
                <h3>Add Exercises</h3>
                <select
                    value={filterExerciseType}
                    id="filter-type"
                    onChange={(e) => filterExerciseList(e)}
                    className="form-select"
                >
                    <option>-- Filter Exercise Type -- </option>
                    {filterExerciseType && <option value="">All</option>}
                    <option value="push">Push</option>
                    <option value="pull">Pull</option>
                    <option value="legs">Legs</option>
                </select>
                <br></br>
                <div className="row row-cols-1 row-cols-md-3 g-4">
                    {filterExercises.length > 0
                        ? filterExercises.map((exercise) => {
                            return (
                                <ExerciseForm
                                    key={exercise.id}
                                    exercise={exercise}
                                    onCreate={handleCreateExercise}
                                />
                            )
                        })
                    : exercises.map((exercise) => {
                            return (
                                <ExerciseForm
                                    key={exercise.id}
                                    exercise={exercise}
                                    onCreate={handleCreateExercise}
                                />
                            )
                        })}
                </div>
            </div>
        </>
    )
}

export default CreateWorkout

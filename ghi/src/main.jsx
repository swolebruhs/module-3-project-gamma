import React from 'react'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { store } from './app/store'
import { Provider } from 'react-redux'
import Error from './app/pages/error'
import Register from './app/pages/Register'
import WorkoutDetails from "./app/pages/createWorkoutSlice"
import LandingPage from './app/pages/LandingPage.jsx'
import Login from './app/pages/Login.jsx'
import CreateWorkout from './app/pages/CreateWorkout'
import CalendarPage from './app/pages/CalendarView'
import WorkoutList from './app/pages/workoutlist'

const router = createBrowserRouter([
    {
        path: '/',
        element: <App />,
        errorElement: <Error />,
        children: [
            { index: true, element: <LandingPage /> },
            { path: 'register', element: <Register /> },
            { path: 'workouts/create', element: <CreateWorkout />},
            { path: 'workouts/:id', element: <WorkoutDetails />},
            { path: 'workouts/create', element: <CreateWorkout />},
            { path: 'login', element: <Login />},
            { path: 'calendar-view', element: <CalendarPage />},
            { path: 'workouts', element: <WorkoutList />}
        ],
    },
])

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <Provider store={store}>
            <RouterProvider router={router} />
        </Provider>
    </React.StrictMode>
)

⠀FRIDAY 03/22/2024

Day 1 - February 27, 2024:

	Our team worked together today to configure our FastAPI backend's authentication utilizing Galvanize's unique backend authentication module in conjunction with JWT. Using Zoom screen sharing and VS Code LiveShare, we collaborated during mob coding sessions while adhering to the JWTDOWN tutorial. We made progress setting up authentication, but we still haven't integrated the database. Our project's README file is still a work in progress, but we also started writing it. The process of monitoring issues has begun with the opening of our first problem. Our Mod3 project, Swole Brothers, is now officially starting with these first stages!



Day 2 - February 28, 2024:
	Our team made progress today implementing JWTDOWN authentication for our FastAPI backend. We talked about our progress, future goals, and any roadblocks at our standup. Mob building the backend authentication over Zoom screen sharing and VS Code LiveShare was what we proceeded to do. We are making steady progress, laying a solid basis for user authentication—a crucial component of online applications—despite the double difficulty of learning and implementing backend authentication at the same time. This ability will come in very handy in the future when creating new authentication systems or adjusting to new ones may be necessary in the workplace. By basing our Accounts router, queries, and models on the JWTDown Library, we were able to incorporate its functionality. Our authentication endpoints are now prepared for database interaction, having undergone testing on FASTAPI DOCS SWAGGER.


Day 3 - February 29, 2024:
	On day 3, we mob programmed again to continue building our backend user authentication. On this day, we were able to get the log in and log out routers working which was great and refreshing because we were slowly but surely getting the hand of it. Forgot to add that we did start building our migration tables using postgresql and began making those queries functions as well as our pydantic models. The way we decided to go about things when it came to development was create migration tables, define our pydantic models, create the routers and then start the query functions.


Day 4 - March 1, 2024:
	At this point in time, we had the user authentication setup which included the routers, queries, postgresql tables and pydantic models. So as a team, we start brain storming ideas on how we’ll insert exercises in our database. We thought of two options, first option would be to create every exercise out there and manually type in each exercise (this wouldn’t be convenient at all) the other option was to use a 3rd party api to integrate all these exercises in our database so we decided to use RapidAPI to add all these exercises. Aside from that, we added the pydantic	models for the exercises, created the table for exercises and began the query functions as well as the routers. At this point in time, we have the get list of exercise routers setup and the get one exercise query.


Day 5 - March 04, 2024:
	Our team spent today configuring the database schema for our fitness application, including exercise tables with fields for equipment, target muscle, body group, name, id, and photo URL. Additionally, we started developing queries to save workouts in our database and retrieve them from an external API. In order to obtain a particular exercise by ID, show all exercises, and build exercises depending on target muscles, we established APIs in our FastAPI backend. To guarantee that only users who are logged in may access these endpoints, we added authentication. All in all, it was a fruitful session as we established the foundation for the main features of our app.


Day 6 - March 05, 2024:
	Today, our team kept utilizing FastAPI to work on the backend of our fitness app. We put up routers for controlling user accounts, workouts, and exercise slices in addition to integrating authentication and CORS middleware. Exercise names, target muscles, sets, reps, is_complete, and completed_on are just a few of the fields that we included in our database schemas for exercises and exercise slices. In order to create exercise slices and get exercises and exercise slices, we created queries and endpoints. For exercises, we have also begun mapping replies from external APIs to our database structure. All things considered, we made great strides in laying the groundwork for the functionality of our software.



Day 7 - March 06, 2024
	Our team kept working on the FastAPI backend of our exercise app today. Our main goal was to put the exercise management features into place, which included creating database schemas, queries, and endpoints. With fields for the workout ID, sets, reps, is_complete, and completed_on, we developed a schema for workout slices. We put in place queries to generate new exercise slices and retrieve existing workout slices. In our FastAPI app, we also established routes to manage the creation and retrieval of workouts. To guarantee that only users who are logged in may access these endpoints, we added authentication. All things considered, we made great strides in developing the backend features needed to manage workouts within our app.


Day 8 - March 07, 2024
	Today, our group worked on enhancing the error handling in our workout app's backend. We focused on improving the clarity and usefulness of error messages by refactoring them to provide more descriptive and informative feedback to users. Additionally, we implemented logging functionality to track errors and gather debug information, which will help us identify and resolve issues more efficiently in the future. Overall, these improvements will enhance the user experience and make it easier for us to maintain and debug the backend of our workout app.


Day 9 - March 8, 2024
	Our team kept using FastAPI to work on the backend of our fitness app today. Our main focus was on specifying the endpoints, queries, and database schema needed to manage the exercise slices.

<!-- The `workout_slice} table's schema was first defined, with fields for {id}, `workout_id}, `date_completed}, `scheduled}, `notes}, and `is_complete} included. The details of each workout slice, including the date of completion and any accompanying remarks, will be kept in this table. -->

We then put queries into practice to construct and get workout segments. A list of workout slices, along with information on the related workout, is obtained from the database using the `get` method. In order to add new workout slices to the database, we also developed the `create` method. After that, we put the functionality to edit and remove workout segments into practice. A workout slice can be deleted from the database using the delete method, however existing exercise slices can be modified by users using the update method.

To further guarantee that only users who are logged in can access these endpoints, we further incorporated authentication. This security feature aids in safeguarding the private exercise information kept within our software.

All things considered, we made great strides toward creating the backend features needed to control the workout slices within our app. In order to make sure these features function flawlessly for our users, we intend to keep improving and testing them.



Day 10 - March 11, 2024
	We developed an exercise app as our group project, paying close attention to both the frontend and backend. We created components for presenting workout lists and managing user authentication on the front end using React with Redux Toolkit for state management. Redux Toolkit's `createApi` method was utilized to incorporate API query logic into the TypeScript/JavaScript code. We worked on database interactions to retrieve workout and exercise data, generate workout slices, and manage API requests for the Python-implemented backend. Our overall objective was to develop a useful web application that prioritizes user authentication and a seamless workout management experience.


Day 11 - March 12, 2024
	- Our group made great strides toward the fitness app project today. Using FastAPI, we carried on creating the backend while concentrating on exercise management. This included creating queries, endpoints, and database schemas, including one for exercise slices. Additionally, we enhanced the backend's error handling and added authentication. I used React and Redux to work on the calendar component on the front end, which allows users to see workouts according to their completion dates. The project is coming along nicely, and I can't wait to see it continue.


Day 12 - March 13, 2024
	For our project, our group's main emphasis today was frontend development. We kept working on the navigation bar element, putting conditional rendering into practice to show various links according to the user's authentication state. We made sure that, depending on whether the user was logged in or not, the navigation bar displayed the correct links for Home, Exercise, Login, and Logout using React Router. To enhance the navigation bar's look and usability, we also worked on customizing it. On the registration form, we also made progress. We implemented form validation to make sure the passwords match before letting the user submit the form. All in all, today's work was focused on improving our application's frontend features and user experience.



Day 13 - March 14, 2024
	Our team spent today working on adding new features to our fitness app. To improve the management of exercise slice data, we created models for input, output, and update models. Additionally, we focused on frontend elements to show workout and activity details dynamically, such WorkoutDetails and WorkoutSliceDetails. We also included features that allow customers to finish exercises, such note-taking and changing workout slice data. To control navigation and provide a seamless user experience, we used React Router. All things considered, our team made great strides in improving our exercise app's functionality and user design.


Day 14 - March 16, 2024
	Our team worked on improving the user login and navigation of our exercise app today. Redux Toolkit's `useLoginMutation} was utilized to manage the login functionality on our constructed login page, which included form validation and error handling. Additionally, we developed a navbar component that uses `useGetUserTokenQuery` and `useLogoutMutation` to control the user's login state and dynamically displays navigation links based on the authentication status of the user. We also styled the landing page and navbar to enhance the user experience overall. All things considered, our team made great strides in enhancing the frontend features and user experience of our fitness software.


Day 15 - March 17, 2024
	Our team spent today putting several features for our fitness app into practice. To handle the exercise slice data more efficiently, we created models for input, output, and update models. In order to dynamically show workout and activity information, we also worked on frontend components like WorkoutDetails and WorkoutSliceDetails. Furthermore, we included features that allow users to finish exercises, such as adding notes and changing workout slice data. React Router was used to control navigation and provide a seamless user experience. All things considered, our team made great strides in improving the features and layout of our fitness software.


Day 16 - March 18, 2024
	As for this date, we were well into our frontend. I did some research on how to implement the calendar view and found some calendar implementation that came from react. We were also at a point where we split into groups and did pair programming and worked on separate components. Added components like the navbar and used some basic styling using bootstrap.


Day 17 - March 19, 2024
	We worked together to configure a Dockerized FastAPI backend, concentrating on error handling, database integration, and API endpoints for a fitness app. Preparing and entering workout data from a CSV file into our database was one of the main jobs we completed. We read the exercise data from the CSV file using Python's CSV module, and then we used SQLAlchemy to run INSERT queries that mapped the values from the CSV file to the columns in the database table. This preprocessing stage was essential to getting the first exercise data into our database, which allowed the app to offer a wide variety of exercises for users to pick from when building routines.



Day 18 - March 20, 2024
	As a group, we worked on enhancing our workout app by implementing a landing page with key features, such as logging daily workouts, creating custom workouts, and displaying recommended workouts. We also integrated user authentication to personalize the experience. Additionally, we improved the backend by pre-processing CSV data to populate the database with exercise and workout information, ensuring a robust selection of activities for users. Our collaboration involved frontend and backend development, emphasizing usability and functionality to create a comprehensive fitness tracking solution.



Day 19 - March 21, 2024
	Today, our team worked hard to improve our project's error handling systems. To make sure it reacts correctly to different problem circumstances, such illegal access or resources not found, we carefully examined every API endpoint. We improved our error messages to make them more instructive and descriptive in an effort to give users precise instructions on how to fix problems. In order to monitor and troubleshoot the program in production, we also built logging to track problems and debug information. Our emphasis on error handling is a reflection of our dedication to providing our application's users with a dependable and intuitive experience.

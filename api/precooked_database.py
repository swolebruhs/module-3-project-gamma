import csv
from queries.pool import pool
import ast


def precook_exercises():
    try:
        with pool.connection() as conn:
            with conn.cursor() as db:
                with open(
                    "cooked_exercises/exercises.csv", "r"
                ) as exercise_csv:
                    reader = csv.DictReader(exercise_csv)
                    for row in reader:
                        try:
                            db.execute(
                                """
                            INSERT INTO exercise
                            (
                            id,
                            name,
                            body_group,
                            body_part,
                            equipment,
                            pic_url,
                            target,
                            descriptions )
                            VALUES
                                (%s, %s, %s, %s, %s, %s, %s, %s)
                            RETURNING id;
                            """,
                                [
                                    row["id"],
                                    row["name"],
                                    row["body_group"],
                                    row["body_part"],
                                    row["equipment"],
                                    row["pic_url"],
                                    row["target"],
                                    row["descriptions"],
                                ],
                            )
                        except Exception:
                            continue
    except Exception as e:
        print(e)


def precook_exercise_slices():
    try:
        with pool.connection() as conn:
            with conn.cursor() as db:
                with open(
                    "cooked_exercises_slices/exerciseslices.csv", "r"
                ) as exercise_slice_csv:
                    reader = csv.DictReader(exercise_slice_csv)
                    for row in reader:
                        try:
                            db.execute(
                                """
                                INSERT INTO exercise_slice
                                (
                                id,
                                exercise_id,
                                sets,
                                reps
                                )
                                VALUES
                                (%s, %s, %s, %s)
                                RETURNING id;
                                """,
                                [
                                    row["id"],
                                    row["exercise_id"],
                                    row["sets"],
                                    row["reps"],
                                ],
                            )
                        except Exception:
                            continue
    except Exception as e:
        print(e)


def precook_workouts():
    try:
        with pool.connection() as conn:
            with conn.cursor() as db:
                with open("cooked_workouts/workouts.csv", "r") as workout_csv:
                    reader = csv.DictReader(workout_csv)
                    for row in reader:
                        try:
                            print("workouts", row)
                            result = db.execute(
                                """
                                INSERT INTO workout
                                (
                                name,
                                ppl
                                )
                                VALUES
                                (%s, %s)
                                RETURNING id;
                                """,
                                [row["name"], row["ppl"]],
                            )
                            workout_id = result.fetchone()[0]
                            for exercise in ast.literal_eval(
                                row["exercise_slice_ids"]
                            ):
                                db.execute(
                                    """
                                    INSERT INTO workout_exercises
                                    (workout_id, exercise_slice_id)
                                    VALUES
                                    (%s, %s)
                                    RETURNING id;
                                    """,
                                    [workout_id, exercise],
                                )

                        except Exception:
                            continue
    except Exception as e:
        print(e)


if __name__ == "__main__":
    precook_exercises()
    precook_exercise_slices()
    precook_workouts()

from models.exercises import ExerciseOut, TargetMuscle, Muscle
from models.errors import HttpError
from utility_functions.exercises import push_pull_legs
from .pool import pool
from typing import Union, List
import requests
import os


class ExercisesQueries:

    def get_one(self, id: int) -> Union[ExerciseOut, HttpError]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , name
                        , body_group
                        , body_part
                        , equipment
                        , pic_url
                        , target
                        , descriptions
                        FROM exercise
                        WHERE id = %s
                        """,
                        [id],
                    )
                    exercise = result.fetchone()
                    if exercise is None:
                        return HttpError(detail="Exercise not found")
                    return ExerciseOut(
                        id=exercise[0],
                        name=exercise[1],
                        body_group=exercise[2],
                        body_part=exercise[3],
                        equipment=exercise[4],
                        pic_url=exercise[5],
                        target=Muscle(name=exercise[6]),
                        descriptions=exercise[7],
                    )
        except Exception as e:
            print(e)
            return HttpError(detail="Exercise not found")

    def get(self) -> Union[List[ExerciseOut], HttpError]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    results = db.execute(
                        """
                        SELECT id
                        , name
                        , body_group
                        , body_part
                        , equipment
                        , pic_url
                        , target
                        , descriptions
                        FROM exercise;
                        """
                    )
                    exercises = []
                    for result in results:
                        e = ExerciseOut(
                            id=result[0],
                            name=result[1],
                            body_group=result[2],
                            body_part=result[3],
                            equipment=result[4],
                            pic_url=result[5],
                            target=Muscle(name=result[6]),
                            descriptions=result[7],
                        )
                        exercises.append(e)
                    return exercises
        except Exception as e:
            print(e)
            return HttpError(detail="Exercises not found")

    def create(
        self, info: TargetMuscle
    ) -> Union[List[ExerciseOut], HttpError]:

        url = (
            f"https://exercisedb.p.rapidapi.com/exercises/bodyPart/{info.name}"
        )
        querystring = {"limit": info.limit}
        headers = {
            "X-RapidAPI-Key": os.environ.get("EXERCISE_DB_KEY"),
            "X-RapidAPI-Host": "exercisedb.p.rapidapi.com",
        }
        response = requests.get(url, headers=headers, params=querystring)
        exercises_list = response.json()

        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    exercises_out = []
                    for e in exercises_list:
                        des = ""
                        for i in e[
                            "instructions"
                        ]:  
                            des += i + " "
                        ppl = push_pull_legs(e["target"])
                        if ppl != None:
                            try:
                                if type(self.get_one(e["id"])) != HttpError:
                                    continue
                                else:
                                    result = db.execute(
                                        """
                                        INSERT INTO exercise
                                        (
                                        id,
                                        name,
                                        body_group,
                                        body_part,
                                        equipment,
                                        pic_url,
                                        target,
                                        descriptions )
                                        VALUES
                                            (%s, %s, %s, %s, %s, %s, %s, %s)
                                        RETURNING id;
                                        """,
                                        [
                                            e["id"],
                                            e["name"],
                                            ppl,
                                            e["bodyPart"],
                                            e["equipment"],
                                            e["gifUrl"],
                                            e["target"],
                                            des,
                                        ],
                                    )
                                    exercise = result.fetchone()

                                    exercises_out.append(
                                        ExerciseOut(
                                            id=exercise[0],
                                            name=e["name"],
                                            body_group=ppl,
                                            body_part=e["bodyPart"],
                                            equipment=e["equipment"],
                                            pic_url=e["gifUrl"],
                                            target=Muscle(name=e["target"]),
                                            descriptions=des,
                                        )
                                    )
                            except Exception as e:
                                print(e)
                                return HttpError(
                                    detail="Could not create exercise."
                                )
            return exercises_out

        except Exception as e:
            print(e)
            return HttpError(detail="Could not create exercise.")

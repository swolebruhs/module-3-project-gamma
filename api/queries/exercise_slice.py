from models.exercise_slice import (
    ExerciseSliceOut,
    ExerciseSliceIn,
    ExerciseSliceUpdate,
)
from models.errors import HttpError
from .pool import pool
from typing import Union, List
from queries.exercises import ExercisesQueries


class ExerciseSliceQueries:
    def get_one(self, id: int) -> Union[ExerciseSliceOut, HttpError]:
        try:
            exercise_queries = ExercisesQueries()
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , exercise_id
                        , sets
                        , reps
                        , is_complete
                        , completed_on
                        FROM exercise_slice
                        WHERE id = %s
                        """,
                        [id],
                    )

                    exercise_slice = result.fetchone()

                    if exercise_slice is None:
                        return HttpError(detail="exercise slice not found")
                    exercise_out = exercise_queries.get_one(
                        id=exercise_slice[1]
                    )
                    if exercise_slice[5] is None:
                        return ExerciseSliceOut(
                            id=exercise_slice[0],
                            exercise=exercise_out,
                            sets=exercise_slice[2],
                            reps=exercise_slice[3],
                            is_complete=exercise_slice[4],
                        )
                    return ExerciseSliceOut(
                        id=exercise_slice[0],
                        exercise=exercise_out,
                        sets=exercise_slice[2],
                        reps=exercise_slice[3],
                        is_complete=exercise_slice[4],
                        completed_on=exercise_slice[5],
                    )
        except Exception as e:
            print(e)
            return HttpError(detail="exercise slice not found")

    def get(self) -> Union[List[ExerciseSliceOut], HttpError]:
        exercise_queries = ExercisesQueries()
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    results = db.execute(
                        """
                        SELECT id
                        , exercise_id
                        , sets
                        , reps
                        , is_complete
                        , completed_on
                        FROM exercise_slice;
                        """
                    )

                    exercise_slice_list = []
                    for result in results:
                        exercise_out = exercise_queries.get_one(id=result[1])
                        exercise_slice_out = ExerciseSliceOut(
                            id=result[0],
                            exercise=exercise_out,
                            sets=result[2],
                            reps=result[3],
                            is_complete=result[4],
                            completed_on=result[5],
                        )
                        exercise_slice_list.append(exercise_slice_out)
                    return exercise_slice_list
        except Exception as e:
            print(e)
            return HttpError(detail="exercise slice not found")

    def create(
        self,
        exercise_slice: ExerciseSliceIn,
    ) -> Union[ExerciseSliceOut, HttpError]:
        exercise_queries = ExercisesQueries()
        with pool.connection() as conn:
            with conn.cursor() as db:
                try:
                    result = db.execute(
                        """
                        INSERT INTO exercise_slice
                            (exercise_id, sets, reps)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            exercise_slice.exercise_id,
                            exercise_slice.sets,
                            exercise_slice.reps,
                        ],
                    )
                    id = result.fetchone()[0]

                    exercise_out = exercise_queries.get_one(
                        id=exercise_slice.exercise_id
                    )

                    return ExerciseSliceOut(
                        id=id,
                        exercise=exercise_out,
                        sets=exercise_slice.sets,
                        reps=exercise_slice.reps,
                    )
                except Exception as e:
                    print(e)
                    return HttpError(
                        detail="Exercise slice could not be created."
                    )

    def update(
        self,
        exercise_slice_id: int,
        exercise_slice_update: ExerciseSliceUpdate,
    ) -> Union[ExerciseSliceOut, HttpError]:
        exercise_queries = ExercisesQueries()
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE exercise_slice
                        SET
                            exercise_id = %s,
                            sets = %s,
                            reps = %s,
                            is_complete = %s,
                            completed_on = %s
                        WHERE id = %s
                        """,
                        [
                            exercise_slice_update.exercise_id,
                            exercise_slice_update.sets,
                            exercise_slice_update.reps,
                            exercise_slice_update.is_complete,
                            exercise_slice_update.completed_on,
                            exercise_slice_id,
                        ],
                    )
                    exercise_out = exercise_queries.get_one(
                        id=exercise_slice_update.exercise_id
                    )

                    return ExerciseSliceOut(
                        id=exercise_slice_id,
                        exercise=exercise_out,
                        sets=exercise_slice_update.sets,
                        reps=exercise_slice_update.reps,
                        is_complete=exercise_slice_update.is_complete,
                        completed_on=exercise_slice_update.completed_on,
                    )

        except Exception as e:
            print(e)
            return HttpError(detail="exercise slice cannot be updated")

    def delete(self, exercise_slice_id: int) -> bool:
        try:
            is_deleted = self.get_one(exercise_slice_id)
            if is_deleted is None:
                return False

            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM exercise_slice
                        WHERE id = %s
                        """,
                        [exercise_slice_id],
                    )
                    return True

        except Exception as e:
            print(e)
            return HttpError(detail="Could not be deleted")

from datetime import datetime
from queries.workouts import WorkoutQueries
from models.workout_slice import (
    WorkoutSliceIn,
    WorkoutSliceOut,
    WorkoutSliceUpdate,
)
from .pool import pool
from typing import Union, List
from models.errors import HttpError


class WorkoutSliceQueries:

    def get_users_last_workout(
        self, user_id: int
    ) -> Union[WorkoutSliceOut, HttpError, None]:
        workout_queries = WorkoutQueries()
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , workout_id
                        , date_complete
                        , scheduled
                        , notes
                        , is_complete
                        , user_id
                        FROM workout_slice
                        WHERE user_id = %s
                        ORDER BY date_complete DESC
                        """,
                        [user_id],
                    )
                    workout = result.fetchone()
                    if workout is None:
                        return None
                    workout_out = workout_queries.get_one(id=workout[1])

                    return WorkoutSliceOut(
                        workout_slice_id=workout[0],
                        workout=workout_out,
                        date_complete=workout[2],
                        scheduled=workout[3],
                        notes=workout[4],
                        is_complete=workout[5],
                        user_id=workout[6],
                    )

        except Exception as e:
            print(e)
            return HttpError(detail="Workout slice not found")

    def get_one(self, id: int) -> Union[WorkoutSliceOut, HttpError]:
        try:
            workout_queries = WorkoutQueries()
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , workout_id
                        , date_complete
                        , scheduled
                        , notes
                        , is_complete
                        , user_id
                        FROM workout_slice
                        WHERE id = %s
                        """,
                        [id],
                    )
                    workout_slice = result.fetchone()
                    if workout_slice is None:
                        return HttpError(detail="Workout slice not found")
                    workout_out = workout_queries.get_one(id=workout_slice[1])

                    return WorkoutSliceOut(
                        workout_slice_id=workout_slice[0],
                        workout=workout_out,
                        date_complete=workout_slice[2],
                        scheduled=workout_slice[3],
                        notes=workout_slice[4],
                        is_complete=workout_slice[5],
                        user_id=workout_slice[6],
                    )
        except Exception as e:
            print(e)
            return HttpError(detail="Workout slice not found")

    def get(self) -> Union[List[WorkoutSliceOut], HttpError]:
        workout_queries = WorkoutQueries()
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    results = db.execute(
                        """
                        SELECT id,
                        workout_id,
                        date_complete,
                        scheduled,
                        notes,
                        is_complete,
                        user_id
                        FROM workout_slice;
                        """
                    )
                    workout_slice = []
                    for result in results:
                        workout_out = workout_queries.get_one(id=result[1])
                        workout_slice_out = WorkoutSliceOut(
                            workout_slice_id=result[0],
                            workout=workout_out,
                            date_complete=result[2],
                            scheduled=result[3],
                            notes=result[4],
                            is_complete=result[5],
                            user_id=result[6],
                        )

                        workout_slice.append(workout_slice_out)
                    return workout_slice
        except Exception as e:
            print(e)
            return HttpError(detail=f"Cannot get list of workout slices, {e}")

    def create(
        self, workout_slice: WorkoutSliceIn, account_id: int
    ) -> Union[WorkoutSliceOut, HttpError]:
        workout_queries = WorkoutQueries()
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO workout_slice
                            (workout_id
                            , scheduled
                            , notes
                            , is_complete
                            , date_complete
                            , user_id)
                        VALUES
                            (%s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            workout_slice.workout_id,
                            workout_slice.scheduled,
                            workout_slice.notes,
                            workout_slice.is_complete,
                            datetime.now(),
                            account_id,
                        ],
                    )
                    id = result.fetchone()[0]

                    workout_out = workout_queries.get_one(
                        id=workout_slice.workout_id
                    )

                    return WorkoutSliceOut(
                        workout_slice_id=id,
                        workout=workout_out,
                        scheduled=workout_slice.scheduled,
                        is_complete=workout_slice.is_complete,
                        notes=workout_slice.notes,
                        date_complete=datetime.now(),
                        user_id=account_id,
                    )

        except Exception as e:
            print(e)
            return HttpError(detail="workout slice cannot be created")

    def update(
        self, workout_slice_id: int, workout_slice_update: WorkoutSliceUpdate
    ) -> Union[WorkoutSliceOut, HttpError]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE workout_slice
                        SET
                            date_complete = %s,
                            scheduled = %s,
                            notes = %s,
                            is_complete = %s,
                            workout_id = %s
                        WHERE id = %s
                        """,
                        [
                            workout_slice_update.date_complete,
                            workout_slice_update.scheduled,
                            workout_slice_update.notes,
                            workout_slice_update.is_complete,
                            workout_slice_update.workout_id,
                            workout_slice_id,
                        ],
                    )
                    return self.populate_workout_slice(
                        workout_slice_id, workout_slice_update
                    )

        except Exception as e:
            print(e)
            return HttpError(detail="Workout Slice cannot be updated")

    def delete(self, workout_slice_id: int) -> bool:
        try:
            is_deleted = self.get_one(workout_slice_id)
            if is_deleted is None:
                return False

            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM workout_slice
                        WHERE id = %s
                        """,
                        [workout_slice_id],
                    )
                    return True

        except Exception as e:
            print(e)
            return HttpError(detail="could not be deleted")

    def populate_workout_slice(
        self, workout_slice_id: int, workout_slice: WorkoutSliceUpdate
    ) -> WorkoutSliceOut:
        workout_queries = WorkoutQueries()
        workout_out = workout_queries.get_one(id=workout_slice.workout_id)
        if workout_slice.date_complete is None and workout_slice.scheduled:
            return WorkoutSliceOut(
                workout_slice_id=workout_slice_id,
                workout=workout_out,
                notes=workout_slice.notes,
                is_complete=workout_slice.is_complete,
            )
        elif workout_slice.date_complete is None:
            return WorkoutSliceOut(
                workout_slice_id=workout_slice_id,
                workout=workout_out,
                date_complete=workout_slice.date_complete,
                notes=workout_slice.notes,
                is_complete=workout_slice.is_complete,
            )
        elif workout_slice.scheduled is None:
            return WorkoutSliceOut(
                workout_slice_id=workout_slice_id,
                workout=workout_out,
                scheduled=workout_slice.scheduled,
                notes=workout_slice.notes,
                is_complete=workout_slice.is_complete,
            )
        return WorkoutSliceOut(
            workout_slice_id=workout_slice_id,
            workout=workout_out,
            date_complete=workout_slice.date_complete or None,
            scheduled=workout_slice.scheduled,
            notes=workout_slice.notes,
            is_complete=workout_slice.is_complete,
        )

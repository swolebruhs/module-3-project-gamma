from models.accounts import AccountIn, AccountOutWithPassword
from models.errors import HttpError
from .pool import pool
from typing import Union


class DuplicateAccountError(ValueError):
    pass


class AccountQueries:
    def get(self, email: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , full_name
                        , password
                        , height_in_inches
                        , weight_in_pounds
                        , sex
                        FROM account
                        WHERE email = %s
                        """,
                        [email],
                    )
                    account = result.fetchone()
                    if account is None:
                        return None
                    return AccountOutWithPassword(
                        id=account[0],
                        full_name=account[1],
                        email=email,
                        hashed_password=account[2],
                        height_in_inches=account[3],
                        weight_in_pounds=account[4],
                        sex=account[5],
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not get account"}

    def create(
        self, info: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        if self.get(email=info.email) is not None:
            raise DuplicateAccountError
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO account
                            (
                            email
                            , password
                            , full_name
                            , height_in_inches
                            , weight_in_pounds
                            , sex
                            )
                        VALUES
                            (%s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            info.email,
                            hashed_password,
                            info.full_name,
                            info.height_in_inches,
                            info.weight_in_pounds,
                            info.sex,
                        ],
                    )
                    id = result.fetchone()[0]
                    if id is None:
                        return None
                    return AccountOutWithPassword(
                        id=id,
                        email=info.email,
                        full_name=info.full_name,
                        hashed_password=hashed_password,
                        height_in_inches=info.height_in_inches,
                        weight_in_pounds=info.weight_in_pounds,
                        sex=info.sex,
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not create account"}

    def update(
        self, email: str, info: AccountIn
    ) -> Union[AccountOutWithPassword, HttpError]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE account
                        SET full_name = %s
                        , height_in_inches = %s
                        , weight_in_pounds = %s
                        , sex = %s
                        WHERE email = %s
                        """,
                        [
                            info.full_name,
                            info.height_in_inches,
                            info.weight_in_pounds,
                            info.sex,
                            email,
                        ],
                    )
                    result = db.execute(
                        """
                        SELECT id,
                        full_name,
                        email,
                        height_in_inches,
                        weight_in_pounds,
                        sex,
                        password
                        FROM account
                        WHERE email = %s
                        """,
                        [email],
                    )
                    updated_data = result.fetchone()

                    if updated_data:
                        (
                            id,
                            email,
                            full_name,
                            height_in_inches,
                            weight_in_pounds,
                            sex,
                            password,
                        ) = updated_data
                        return AccountOutWithPassword(
                            id=id,
                            email=email,
                            full_name=full_name,
                            height_in_inches=height_in_inches,
                            weight_in_pounds=weight_in_pounds,
                            sex=sex,
                            hashed_password=password,
                        )

                    else:
                        return HttpError(
                            status_code=404, detail="Account not found"
                        )

        except Exception as e:
            print(e)
            return {"message": "Could not update an account"}

    def delete(self, account_email: str) -> bool:
        try:

            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM account
                        WHERE email = %s
                        """,
                        [account_email],
                    )

                    if db.rowcount == 0:
                        return False
                    return True

        except Exception as e:
            print(e)
            return False

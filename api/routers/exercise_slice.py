from fastapi import (
    Depends,
    Response,
    APIRouter,
)
from queries.exercise_slice import ExerciseSliceQueries
from models.accounts import AccountOut
from typing import Union, List
from models.exercise_slice import (
    ExerciseSliceIn,
    ExerciseSliceOut,
    ExerciseSliceUpdate,
)
from authenticator import authenticator
from models.errors import HttpError


router = APIRouter()


@router.get(
    "/api/exercise_slice/{id}",
    response_model=Union[ExerciseSliceOut, HttpError],
)
async def get_exercise_slice(
    id: int,
    response: Response,
    exercise_slices: ExerciseSliceQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[ExerciseSliceOut, HttpError]:
    if account:
        exercise_slice = exercise_slices.get_one(id=id)
        if type(exercise_slice) is HttpError:
            response.status_code = 404
            return exercise_slice
        return exercise_slice
    response.status_code = 401
    return HttpError(detail="User not logged in")


@router.get(
    "/api/exercise_slice",
    response_model=Union[List[ExerciseSliceOut], HttpError],
)
async def list_exercise_slices(
    response: Response,
    exercise_slices: ExerciseSliceQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
):
    if account:
        exercise_slice_list = exercise_slices.get()
        if type(exercise_slice_list) is HttpError:
            response.status_code = 404
            return exercise_slice_list
        return exercise_slice_list
    response.status_code = 401
    return HttpError(detail="User not logged in")


@router.post(
    "/api/exercise_slice", response_model=Union[ExerciseSliceOut, HttpError]
)
async def create_exercise_slice(
    info: ExerciseSliceIn,
    response: Response,
    exercise_slice: ExerciseSliceQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[ExerciseSliceOut, HttpError]:
    if account:
        exercise_slice = exercise_slice.create(exercise_slice=info)
        if type(exercise_slice) is HttpError:
            response.status_code = 404
            return exercise_slice
        return exercise_slice
    response.status_code = 401
    return HttpError(detail="User not logged in")


@router.delete(
    "/api/exercise_slice/{id}", response_model=Union[bool, HttpError]
)
def delete_exercise_slice(
    exercise_slice_id: int,
    response: Response,
    exercise_slice: ExerciseSliceQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[bool, HttpError]:
    if account:
        is_deleted = exercise_slice.delete(exercise_slice_id)
        if type(is_deleted) is HttpError:
            response.status_code = 404
            return is_deleted
        return is_deleted
    response.status_code = 401
    return HttpError(detail="User not logged in")


@router.put(
    "/api/exercise_slice/{id}",
    response_model=Union[ExerciseSliceOut, HttpError],
)
def update_exercise_slice(
    exercise_slice_id: int,
    response: Response,
    exercise_slice: ExerciseSliceUpdate,
    repo: ExerciseSliceQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[ExerciseSliceOut, HttpError]:
    if account:
        updated_slice = repo.update(exercise_slice_id, exercise_slice)
        if type(updated_slice) is HttpError:
            response.status_code = 404
            return updated_slice
        return updated_slice
    response.status_code = 401
    return HttpError(detail="User not logged in")

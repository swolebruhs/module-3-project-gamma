steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE workout_slice (

            id SERIAL PRIMARY KEY NOT NULL,
            workout_id INTEGER NOT NULL REFERENCES workout(id),
            date_complete TIMESTAMP,
            scheduled TIMESTAMP,
            notes VARCHAR(100),
            is_complete BOOL DEFAULT false,
            user_id INTEGER REFERENCES account(id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE workout_slice;
        """,
    ],
]

steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE exercise (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100) NOT NULL,
            body_part VARCHAR(100) NOT NULL,
            body_group VARCHAR(100) NOT NULL,
            equipment VARCHAR(100) NOT NULL,
            pic_url VARCHAR(200) NOT NULL,
            target VARCHAR(100) NOT NULL,
            descriptions TEXT NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE exercise;
        """,
    ],
]

from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.workouts import WorkoutQueries

client = TestClient(app=app)


def fake_get_current_account_data():
    return {
        # "access_token": "Fake_access_token",
        "account": {
            "id": "1",
            "email": "test@test.com",
            "full_name": "test",
            "height_in_inches": 1,
            "weight_in_pounds": 1,
            "sex": "male",
        }
    }


class FakeWorkoutQueries:
    def get_all(self):
        return []


def test_workout():
    app.dependency_overrides[WorkoutQueries] = FakeWorkoutQueries
    app.dependency_overrides[authenticator.try_get_current_account_data] = (
        fake_get_current_account_data
    )

    res = client.get("/api/workouts")

    assert res.status_code == 200
    assert res.json() == []

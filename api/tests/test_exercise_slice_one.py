from fastapi.testclient import TestClient
from main import app
from queries.exercise_slice import ExerciseSliceQueries
from authenticator import authenticator
from models.exercise_slice import ExerciseSliceOut

client = TestClient(app=app)

def fake_try_get_current_account_data():
    return {
        "id": 1
    }

class ExerciseSliceQueriesMock:
    def get_one(self, id):
        exercise_data = {
            "id": id,
            "exercise": {
                "name": "3/4 sit-up",
                "body_group": "push",
                "body_part": "waist",
                "equipment": "body weight",
                "pic_url": "https://v2.exercisedb.io/image/UvNUmkBR0TsdqU",
                "target": {"name": "abs"},
                "descriptions": "Lie flat on your back with your knees bent and feet flat on the ground. Place your hands behind your head with your elbows pointing outwards. Engaging your abs, slowly lift your upper body off the ground, curling forward until your torso is at a 45-degree angle. Pause for a moment at the top, then slowly lower your upper body back down to the starting position. Repeat for the desired number of repetitions.",
                "id": 1
            },
            "sets": 3,
            "reps": 10,
            "is_complete": False,
            "completed_on": None
        }
        return ExerciseSliceOut(**exercise_data)

def test_get_exercise_slice():
    # arrange
    id = 1
    app.dependency_overrides[ExerciseSliceQueries] = ExerciseSliceQueriesMock
    # act
    result = client.get(f"/api/exercise_slice/{id}")
    print(result.json())  # Add this line for debugging
    # assert
    assert result.status_code == 200
    assert result.json() == {
        "id": 1,
        "exercise": {
            "name": "3/4 sit-up",
            "body_group": "push",
            "body_part": "waist",
            "equipment": "body weight",
            "pic_url": "https://v2.exercisedb.io/image/UvNUmkBR0TsdqU",
            "target": {"name": "abs"},
            "descriptions": "Lie flat on your back with your knees bent and feet flat on the ground. Place your hands behind your head with your elbows pointing outwards. Engaging your abs, slowly lift your upper body off the ground, curling forward until your torso is at a 45-degree angle. Pause for a moment at the top, then slowly lower your upper body back down to the starting position. Repeat for the desired number of repetitions.",
            "id": 1
        },
        "sets": 3,
        "reps": 10,
        "is_complete": False,
        "completed_on": None
    }

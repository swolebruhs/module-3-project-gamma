from pydantic import BaseModel


class TargetMuscle(BaseModel):
    name: str
    limit: int


class Muscle(BaseModel):
    name: str


class ExerciseIn(BaseModel):
    name: str
    body_group: str
    body_part: str
    equipment: str
    pic_url: str
    target: Muscle
    descriptions: str


class ExerciseOut(ExerciseIn):
    id: int
